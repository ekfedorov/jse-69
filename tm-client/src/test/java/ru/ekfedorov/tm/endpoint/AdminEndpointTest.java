package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

public class AdminEndpointTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    private final AdminEndpoint adminEndpoint = context.getBean(AdminEndpoint.class);

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
    }

    @After
    @SneakyThrows
    public void after() {
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void listSession() {
        Assert.assertFalse(adminEndpoint.listSession(sessionAdmin).isEmpty());
    }

}
