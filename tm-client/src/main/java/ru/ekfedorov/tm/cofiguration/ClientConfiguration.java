package ru.ekfedorov.tm.cofiguration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.ekfedorov.tm.endpoint.*;

@ComponentScan("ru.ekfedorov.tm")
public class ClientConfiguration {

    @Bean
    @NotNull
    public final AdminEndpointService adminEndpointService() {
        return new AdminEndpointService();
    }

    @Bean
    @NotNull
    public final AdminEndpoint adminEndpoint(
            @NotNull @Autowired AdminEndpointService adminEndpointService
    ) {
        return adminEndpointService.getAdminEndpointPort();
    }

    @Bean
    @NotNull
    public final AdminUserEndpointService adminUserEndpointService() {
        return new AdminUserEndpointService();
    }

    @Bean
    @NotNull
    public final AdminUserEndpoint adminUserEndpoint(
            @NotNull @Autowired AdminUserEndpointService adminUserEndpointService
    ) {
        return adminUserEndpointService.getAdminUserEndpointPort();
    }

    @Bean
    @NotNull
    public final ProjectEndpointService projectEndpointService() {
        return new ProjectEndpointService();
    }

    @Bean
    @NotNull
    public final ProjectEndpoint projectEndpoint(
            @NotNull @Autowired ProjectEndpointService projectEndpointService
    ) {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Bean
    @NotNull
    public final SessionEndpointService sessionEndpointService() {
        return new SessionEndpointService();
    }

    @Bean
    @NotNull
    public final SessionEndpoint sessionEndpoint(
            @NotNull @Autowired SessionEndpointService sessionEndpointService
    ) {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Bean
    @NotNull
    public final TaskEndpointService taskEndpointService() {
        return new TaskEndpointService();
    }

    @Bean
    @NotNull
    public final TaskEndpoint taskEndpoint(
            @NotNull @Autowired TaskEndpointService taskEndpointService
    ) {
        return taskEndpointService.getTaskEndpointPort();
    }

    @Bean
    @NotNull
    public final UserEndpointService userEndpointService() {
        return new UserEndpointService();
    }

    @Bean
    @NotNull
    public final UserEndpoint userEndpoint(
            @NotNull @Autowired UserEndpointService userEndpointService
    ) {
        return userEndpointService.getUserEndpointPort();
    }

}
