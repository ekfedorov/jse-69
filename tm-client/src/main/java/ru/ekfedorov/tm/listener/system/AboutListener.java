package ru.ekfedorov.tm.listener.system;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.api.service.IPropertyService;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;

@Component
public final class AboutListener extends AbstractListener {

    @NotNull
    @Override
    public String commandArg() {
        return "-a";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Display developer info.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "about";
    }

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    @SneakyThrows
    @EventListener(condition = "@aboutListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[ABOUT]");
        System.out.println(propertyService.getDeveloperName());
        System.out.println(propertyService.getDeveloperEmail());
    }

}
