package ru.ekfedorov.tm.listener.user;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractUserListener;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.empty.UserNotFoundException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

@Component
public final class UserLoginListener extends AbstractUserListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Do login.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "login";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@userLoginListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        Session session = sessionEndpoint.openSession(login, password);
        if (session == null) throw new UserNotFoundException();
        sessionService.setSession(session);
    }

}
