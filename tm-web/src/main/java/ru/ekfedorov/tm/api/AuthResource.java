package ru.ekfedorov.tm.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RequestMapping("/auth")
public interface AuthResource {

    @GetMapping("/login")
    public Boolean login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @GetMapping("/logout")
    public Boolean logout();

}
