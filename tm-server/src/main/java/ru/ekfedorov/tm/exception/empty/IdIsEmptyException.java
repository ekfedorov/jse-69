package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class IdIsEmptyException extends AbstractException {

    public IdIsEmptyException() {
        super("Error! Id is empty...");
    }

}
