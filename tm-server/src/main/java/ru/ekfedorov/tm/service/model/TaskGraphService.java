package ru.ekfedorov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ekfedorov.tm.repository.model.ITaskGraphRepository;
import ru.ekfedorov.tm.repository.model.IUserGraphRepository;
import ru.ekfedorov.tm.api.service.model.ITaskGraphService;
import ru.ekfedorov.tm.enumerated.Sort;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.empty.*;
import ru.ekfedorov.tm.exception.incorrect.IncorrectIndexException;
import ru.ekfedorov.tm.exception.system.NullComparatorException;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.model.TaskGraph;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;
import static ru.ekfedorov.tm.util.ValidateUtil.notNullOrLessZero;

@Service
public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @SneakyThrows
    @Override
    public @NotNull TaskGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (isEmpty(description)) throw new DescriptionIsEmptyException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        task.setUser(userGraphRepository.getOne(userId));
        taskGraphRepository.save(task);
        return task;
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
            taskGraphRepository.removeByUserId(userId);
    }

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public List<TaskGraph> findAll(@Nullable final String userId) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        return taskGraphRepository.findAllByUserId(userId);
    }

    @SneakyThrows
    @NotNull
    @Override
    public Optional<TaskGraph> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskGraphRepository.findOneByUserIdAndId(userId, id);
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<TaskGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        return Optional.ofNullable(
                taskGraphRepository.findAllByUserId(userId).get(index)
        );
    }

    @NotNull
    @SneakyThrows
    @Override
    public Optional<TaskGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        return taskGraphRepository.findOneByUserIdAndName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) throw new NullObjectException();
            taskGraphRepository.deleteById(entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final TaskGraph entity
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (entity == null) throw new NullObjectException();
        taskGraphRepository.removeOneByUserIdAndId(userId, entity.getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        taskGraphRepository.removeOneByUserIdAndId(userId, id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull Optional<TaskGraph> task = findOneByIndex(userId, index);
        if (!task.isPresent()) throw new UserNotFoundException();
        taskGraphRepository.removeOneByUserIdAndId(userId, task.get().getId());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        taskGraphRepository.removeOneByUserIdAndName(userId, name);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        taskGraphRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final TaskGraph task) {
        if (task == null) throw new NullObjectException();
            taskGraphRepository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<TaskGraph> entities) {
        if (entities == null) throw new NullObjectException();
        taskGraphRepository.saveAll(entities);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
            taskGraphRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    public boolean contains(@NotNull final String id) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return taskGraphRepository.existsById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAll() {
        return taskGraphRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAllSort(
            @Nullable final String sort
    ) {
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskGraph> comparator = sortType.getComparator();
            return taskGraphRepository.findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<TaskGraph> findOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new IdIsEmptyException();
        return Optional.of(taskGraphRepository.getOne(id));
    }

    @SneakyThrows
    @Override
    @Transactional
    public void removeOneById(
            @Nullable final String id
    ) {
        if (isEmpty(id)) throw new LoginIsEmptyException();
        taskGraphRepository.deleteById(id);
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        if (status == null) throw new NullObjectException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(status);
        taskGraphRepository.save(entity.get());
    }

    @Override
    @SneakyThrows
    public @NotNull List<TaskGraph> findAll(@Nullable String userId, @Nullable String sort) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(sort)) throw new NullComparatorException();
        @Nullable Sort sortType = Sort.valueOf(sort);
            @NotNull final Comparator<TaskGraph> comparator = sortType.getComparator();
            return taskGraphRepository.findAllByUserId(userId).stream().sorted(comparator).collect(Collectors.toList());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.COMPLETE);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(id)) throw new IdIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (isEmpty(name)) return;
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setStatus(Status.IN_PROGRESS);
        taskGraphRepository.save(entity.get());
    }

    @SneakyThrows
    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (isEmpty(userId)) throw new UserIdIsEmptyException();
        if (notNullOrLessZero(index)) throw new IncorrectIndexException();
        if (isEmpty(name)) throw new NameIsEmptyException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new NullObjectException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        taskGraphRepository.save(entity.get());
    }

}
